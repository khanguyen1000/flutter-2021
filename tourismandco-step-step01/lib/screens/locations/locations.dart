import "package:flutter/material.dart";
import 'package:tourismandco/screens/location_detail/location_detail.dart';
import '../../models/location.dart';
import '../../app.dart';

class Locations extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final locations = Location.fetchAll();

    return Scaffold(
      appBar: AppBar(
        title: Text('Locations'),
      ),
      body: ListView(
        children: locations
            .map(
              (location) => GestureDetector(
                  child: Text(location.name),
                  onTap: () => _onLocationTab(context, location.id)),
            )
            .toList(),
      ),
    );
  }

  _onLocationTab(BuildContext context, int locationID) {
    Navigator.pushNamed(context, LocationDetailRoute,
        arguments: {"id": locationID});
  }
}
